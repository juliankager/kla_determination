function dc = otr(t,c,kla)
%ODE system to calculate oxygentransfer over time
%dc = dc/dt
%t = curretn timestep
% kla = model parameter
%c = curretn concentration (c @ t0 = c0)

c_max = 100;

dc(1) = kla*(c_max-c);

end