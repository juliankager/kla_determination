%% Clear Matlab
clear all
clc

%% Data import
file_name = 'DO_Reaktor1.xlsx'; %set filename
[t,sheet]=xlsfinfo(file_name);  %obtain file info

for i= 1:length(sheet) %loop througth the sheets (tabellenblätter)
current_sheet =  sheet{i} % store current sheetname
[rawdata,Header]=xlsread(file_name,current_sheet); %read Excel sheet and divide it into Data (number format in Excel) and Text

[current_sheet, current_unit]= strtok(current_sheet); % divide current sheetname into Variablename and Unit
Units{i} = current_unit; % write Units on a cell Array
Variables.(current_sheet)= rawdata; % Write data in a struct by naming the struct field like the Ssheet

end


    %%
names=fieldnames(Variables); % obtain list of all structnames

for k = 1: length(names) % loop throutgh the struct
 kla_0 = 25;% set an arbitrary startvalue for kla

 data = Variables.(names{k})(:,2);  % get current data
 time =  Variables.(names{k})(:,1); % get curretn time
 
 kla_opt(k) = fminsearch(@(kla)cost(kla,data ,time),kla_0); %start Fmin optimization to find kla with minimal cost function value

 [t,ysim] = ode23(@(t,c)otr(t,c,kla_opt(k)), time, 0); %solve ODE system for estimated kla (only for Visualization needed)

figure (3) %open/ go to Figure 3

subplot(3,3,k); %create 3*3 subplot and add plot in every iteration
plot(time, data) %create x,y plot
hold on %hold plot to add aditional lines
plot(time, ysim) %add x,y to plot
% some options for the plot
ylabel('DO2 (%)') 
xlabel('Time (h)')
ylim([0,100])
legend('data','sim')
title(['kla = ' num2str(kla_opt(k)) ' h-1'])

end

