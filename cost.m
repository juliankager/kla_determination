function [J] = cost(kla, data, time)
%calculates the cost = Error model and measurement
% J = Error

c0 = 0; %startvalue for ODE system

[t,y] = ode23(@(t,c)otr(t,c,kla), time, c0); %Solve oxygentransprot (otr)

J = sum((y-data).^2) %calutas the sum of the squared error

% Visulizes the current model to data fit
figure (1)
plot(time,data)
hold on
plot(time,y)
hold off
legend('data','sim')
end

